﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TaxAppLab2
{
    public class Taxpayer
    {
        private static ulong taxsCounter;

        private readonly Dictionary<ulong, Tax> Taxs = new Dictionary<ulong, Tax>();

        public string NameOfTax { get; set; }

        public ulong ID { get; set; }

        public string FullName { get; set; }

        public uint PersonalAccount { get; set; }

        public List<Tax> GetTaxs()
        {
            return Taxs.Values.ToList();
        }

        public  Tax GetTax(ulong id)
        {
            Tax tax;
            if (!Taxs.TryGetValue(id, out tax))
            {
                throw new InvalidOperationException();
            }
            return tax;
        }

        public void CreateTax(Tax tax)
        {
            tax.ID = ++taxsCounter;
            Taxs[tax.ID] = tax;
        }
    }
}
