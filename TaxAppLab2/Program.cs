﻿using System;

namespace TaxAppLab2
{
    class Program
    {
        public static void Main()
        {
            var taxApp = new TaxApp();
            var terminal = new TaxTerminal(taxApp); // создание экземпляра класса TaxTermiank - наследника TaxApp
            terminal.ShowMenuTaxApp();
        }
    }
}


