﻿using System;
using System.Collections.Generic;
using System.Text;
using static TaxAppLab2.Tax;

namespace TaxAppLab2
{
    public class TaxTerminal
    {
        private const int INVALID_OPTION = -1;
        private readonly TaxApp taxApp;
   


        public TaxTerminal(TaxApp taxApp)
        {
            this.taxApp = taxApp;
        }

        public void ShowMenuTaxApp()
        {
            var stop = false;
            do
            {
                Console.WriteLine("Меню TaxApp:");
                Console.WriteLine("1. Добавить налогоплательщика");
                Console.WriteLine("2. Показать всех налогоплательщиков");
                Console.WriteLine("3. Выбрать налогоплательщика");
                Console.WriteLine("0. Выход");

                int option = GetMenuOption();
                switch (option)
                {
                    case 1:
                        AddTaxPayer();
                        break;
                    case 2:
                        ShowTaxPayers();
                        break;
                    case 3:
                        SelectTaxPayer();
                        break;
                    case 0:
                        stop = true;
                        break;
                    default:
                        Console.WriteLine("Введите значение меню из списка");
                        break;
                }
            }
            while (!stop);
        }
        private void AddTaxPayer()
        {
            var taxpayer = new Taxpayer
            {
                FullName = ReadText("Введите ФИО клиента : "),
                PersonalAccount = ReadPersonalAccount("Введите его лицевой счет: "),
            };
            taxApp.AddTaxPayer(taxpayer);
            Console.WriteLine($"Налогоплательщик №{taxpayer.ID} {taxpayer.FullName} был успешно добавлен");
        }

        private void ShowTaxPayers()
        {
            List<Taxpayer> taxpayers = taxApp.GetTaxPayers();
            foreach (Taxpayer taxpayer in taxpayers)
            {
                Console.WriteLine($"{taxpayer.ID:d3} {taxpayer.FullName}. Лицевой счет: {taxpayer.PersonalAccount}.");
            }
            Console.WriteLine($"Всего налогоплательщиков в базе: {taxpayers.Count}");
        }

        private void SelectTaxPayer()
        {
            ulong taxpayerID = ReadID("Номер налогоплательщика: ");
            try
            {
                Taxpayer taxpayer = taxApp.GetTaxPayer(taxpayerID);
                ShowTaxPayerMenu(taxpayer);
            }
            catch (InvalidOperationException)
            {
                Console.WriteLine("Налогоплательщика с таким номером не существует");
            }
        }

        // До этого момента все ок!
        private void ShowTaxPayerMenu(Taxpayer taxpayer)
        {
            var stop = false;
            var inn = new Tax();
            do
            {
                Console.WriteLine();
                Console.WriteLine($"Меню налогоплательщика №{taxpayer.ID} {taxpayer.FullName}, лицевой счет №{taxpayer.PersonalAccount}:");
                Console.WriteLine("1. Удалить налогоплательщика");
                Console.WriteLine("2. Добавить налог");
                Console.WriteLine("3. Показать все налоги");
                Console.WriteLine("4. Выбрать налог");
                Console.WriteLine("0. Вернуться назад");
                int option = GetMenuOption();
                switch (option)
                {
                    case 1:
                        if (DeleteClient(taxpayer))
                        {
                            stop = true;
                        }
                        break;
                    case 2:
                        CreateNewTax(taxpayer);
                        break;
                    case 3:
                        ShowTaxs(taxpayer);
                        break;
                    case 4:
                        SelectTax(taxpayer);
                        break;
                    case 0:
                        stop = true;
                        break;
                    default:Console.WriteLine("Введите значение меню из списка");
                        break;
                }
            }
            while (!stop);
        }
        private bool DeleteClient(Taxpayer taxpayer)
        {
            if (ConfirmAction($"Вы действительно хотите удалить клиента №{taxpayer.ID} {taxpayer.FullName}?"))
            {
                taxApp.DeleteTaxPayer(taxpayer.ID);
                Console.WriteLine($"Клиент №{taxpayer.ID} {taxpayer.FullName} был успешно удален");
                return true;
            }

            return false;
        }
        private void CreateNewTax(Taxpayer taxpayer)
        {
            var tax = new Tax
            {
                NameOfTax = ReadText("Введите тип налога: ")
            };
            taxpayer.CreateTax(tax);
            Console.WriteLine($"Налог №{tax.ID} {tax.NameOfTax} был успешно добавлен");
        }
        private void ShowTaxs(Taxpayer taxpayer)
        {
            List<Tax> taxs = taxpayer.GetTaxs();
            foreach (Tax tax in taxs)
            {
                Console.WriteLine($"{tax.ID:d3}. Налог: {tax.NameOfTax}.");
            }
            Console.WriteLine($"Количество налогов в базе: {taxs.Count}");
        }
        private void SelectTax(Taxpayer taxpayer)
        {
            ulong taxID = ReadID("Номер налога: ");
            try
            {
                Tax inn = taxpayer.GetTax(taxID);
                ShowAccountMenu(inn);
            }
            catch (InvalidOperationException)
            {
                Console.WriteLine("Налога с таким номером не существует");
            }
        }
        private void ShowAccountMenu(Tax tax)
        {
            var stop = false;
            do
            {
                Console.WriteLine();
                Console.WriteLine($"Меню лицевого счета");
                Console.WriteLine("1. Начислить сумму налога");
                Console.WriteLine("2. Уплатить налог");
                Console.WriteLine("3. Просмотр истории");
                Console.WriteLine("0. Вернуться назад");

                int option = GetMenuOption();
                switch (option)
                {
                    case 1:
                        taxSum(tax);
                        break;
                    case 2:
                        MakeDeposit(tax);
                        break;
                    case 3:
                        ShowHistory(tax);
                        break;
                    case 0:
                        stop = true;
                        break;
                    default:
                        Console.WriteLine("Введите значение меню из списка");
                        break;
                }
            }
            while (!stop);
        }

        private void taxSum(Tax tax)
        {
            Console.WriteLine("Введите сумму налога");
            ulong a = ulong.Parse(Console.ReadLine());
            var sum = new Tax
            {
                Balance = a
            };
            tax.MakeLoan(a);
            Console.WriteLine($"Сумма введена");
        }

        private void MakeDeposit(Tax tax)
        {
            Console.WriteLine("Введите сумму для уплаты налога");
            ulong a = ulong.Parse(Console.ReadLine());
            var sum = new Tax
            {
                Balance = a
            };
            tax.MakeDeposit(a);
            Console.WriteLine($"Сумма введена");
        }

        private void ShowHistory(Tax tax)
        {     
            List <AccountAction> history = tax.GetHistory();
            foreach (AccountAction name in history)
            {
                Console.WriteLine($"Тип операции - {name.Type}. Сумма операции - {name.Amount}. Дата выполнения операции - {name.Date}");
            }
        }

        private string ReadText(string label)
        {
            string text;
            do
            {
                Console.Write(label);
                text = Console.ReadLine().Trim();
            }
            while (string.IsNullOrEmpty(text));
            return text;
        }

        private uint ReadPersonalAccount(string label)
        {
            uint PersonalAccount;
            string input;
            do
            {
                Console.Write(label);
                input = Console.ReadLine();
            }
            while (!uint.TryParse(input, out PersonalAccount) || PersonalAccount == 0);
            return PersonalAccount;
        }

        private ulong ReadID(string label)
        {
            ulong id;
            string input;
            do
            {
                Console.Write(label);
                input = Console.ReadLine();
            }
            while (!ulong.TryParse(input, out id) || id == 0);
            return id;
        }


        private bool ConfirmAction(string label)
        {
            Console.WriteLine($"{label} (y - да)");
            string input = Console.ReadLine();
            return input == "y" || input == "Y";
        }

        private int GetMenuOption()
        {
            string input = Console.ReadLine();

            int option;
            if (!int.TryParse(input, out option))
            {
                option = INVALID_OPTION;
            }
            return option;
        }
    }
}


