﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaxAppLab2
{
    public class Tax
    {
        private readonly List<AccountAction> history = new List<AccountAction>();

        public ulong ID { get; set; }

        public string NameOfTax { get; set; }

        public bool IsOpen { get; set; }

        public ulong Balance { get; set; }

        public List<AccountAction> GetHistory()
        {
            return history;
        }

        public class AccountAction
        {
            public DateTime Date { get; set; }

            public ActionType Type { get; set; }

            public decimal Amount { get; set; }
        }

        public enum ActionType
        {
            Tax_Charge,
            Tax_Payment
        }

        public void MakeDeposit(ulong amount)
        {
            Balance -= amount;
            WriteHistory(ActionType.Tax_Payment, amount);
        }

        public void MakeLoan(ulong amount)
        {
            Balance += amount;
            WriteHistory(ActionType.Tax_Charge, amount);
        }

        private void WriteHistory(ActionType type, decimal amount)
        {
            var action = new AccountAction
            {
                Date = DateTime.Now,
                Type = type,
                Amount = amount
            };
            history.Add(action);
        }

    }
}
