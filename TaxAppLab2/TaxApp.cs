﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TaxAppLab2
{
    public class TaxApp
    {
        private static ulong taxPayersCounter;

        private readonly Dictionary<ulong, Taxpayer> taxPayers = new Dictionary<ulong, Taxpayer>();

        public List<Taxpayer> GetTaxPayers()
        {
            return taxPayers.Values.ToList();
        }

        public Taxpayer GetTaxPayer(ulong id)
        {
            Taxpayer taxpayer;
            if (!taxPayers.TryGetValue(id, out taxpayer))
            {
                throw new InvalidOperationException();
            }

            return taxpayer;
        }

        public void AddTaxPayer(Taxpayer taxpayer)
        {
            taxpayer.ID = ++taxPayersCounter;
            taxPayers[taxpayer.ID] = taxpayer;
        }

        public void DeleteTaxPayer(ulong id)
        {
            if (!taxPayers.ContainsKey(id))
            {
                throw new InvalidOperationException();
            }

            taxPayers.Remove(id);
        }
    }
}
